import { expect } from "chai";
import { suite, test } from "mocha-typescript";
import { Hanoi } from "./hanoi";

@suite
class HanoiTests {
  @test
  public solve1() {
    const hanoi = new Hanoi(1);
    expect(hanoi.solve().length).to.equal(1);
  }

  @test
  public solve2() {
    const hanoi = new Hanoi(2);
    expect(hanoi.solve().length).to.equal(3);
  }

  @test
  public solve3() {
    const hanoi = new Hanoi(3);
    expect(hanoi.solve().length).to.equal(7);
  }
}

const t = new HanoiTests(); // Make 'declared but never used' error go away
