export class Hanoi {
  private static arrLast<T>(arr: T[]) {
    return arr[arr.length - 1];
  }
  private n: number;
  private towers: number[][];

  constructor(n: number) {
    this.n = n;
    this.towers = [[], [], []];

    for (let i = 0; i < this.n; ++i) {
      this.towers[0].push(this.n - i);
    }
  }

  public solve(): any {
    if (this.n === 0) {
      return [];
    }
    return this.moveN(this.n, 0, 2);
  }

  private move(from: number, to: number): any {
    const x = this.towers[from].pop() as number;
    const destTower = this.towers[to] as number[];
    if (!(destTower.length === 0 || x < Hanoi.arrLast(destTower))) {
      throw new Error(`Bad move trying to move ${x} from ${from} to ${to}`);
    } else {
      // @ts-ignore
      // tslint:disable-next-line:no-console
      console.log(`Moved ${x} from ${from} to ${to}`);
      this.towers[to].push(x); // @ts-ignore
    }
    return { from, to };
  }

  private moveN(n: number, from: number, to: number): any {
    if (n === 0) {
      return [];
    }
    const tmp = [0, 1, 2].find(x => x !== from && x !== to) as number;
    let moves = this.moveN(n - 1, from, tmp);
    moves.push(this.move(from, to));
    moves = moves.concat(this.moveN(n - 1, tmp, to));
    return moves;
  }
}
