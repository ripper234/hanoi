var express = require("express");
var router = express.Router();
import { Hanoi } from "../hanoi";

/* GET home page. */
router.get("/", (req: any, res: any, next: any) => {
  res.render("index", { title: "Hanoi" });
});

router.get("/solve", (req: any, res: any, next: any) => {
  const n = parseInt(req.query.n, 10);
  if (isNaN(n)) {
    res.text = "Missing n";
    res.status(400).json({ error: "Missing n" });
    return;
  }
  const solution = new Hanoi(n).solve();
  res.render("index", { title: "Hanoi", solution });
});

module.exports = router;
